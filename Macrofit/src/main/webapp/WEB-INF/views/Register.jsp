<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/css/signin.css"/>" 
	rel="stylesheet" >
</head>
<body>

	
	
<div class="container">

      <form class="form-signin" action="/register" method="post">
        <h2 class="form-signin-heading">Register</h2>
        <label for="username" class="sr-only">Username</label>
        <input type="text" class="form-control" placeholder="stevesmith" name="username" required autofocus />
        
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="Password" required />
        
        <label for="firstname" class="sr-only">First Name</label>
        <input type="text" class="form-control" placeholder="Steve" name="firstname" required>
        
        <label for="lastname" class="sr-only">Last Name</label>
        <input type="text" class="form-control" placeholder="Smith" name="lastname" required>
        
        <label for="email" class="sr-only">Email</label>
        <input type="email" class="form-control" placeholder="abcd1234@uni.sydney.edu.au" name="email" required> <br>
        
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
         
        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Register">Register</button>
     	 
      </form>
      
      
      

    </div> <!-- /container -->
      
</body>
</html>