<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>

<title>Dashboard</title>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Bootply snippet - Bootstrap 3 Carousel Layout (BS 3)</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description"
	content="This is an example layout with carousel that uses the Bootstrap 3 RC styles. " />
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"
	rel="stylesheet">

<!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="/bootstrap/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="/bootstrap/img/apple-touch-icon-114x114.png">

<style type="text/css">
body {
    padding-top: 70px; /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.img-center {
	margin: 0 auto;
}

footer {
    margin: 50px 0;
}


<!-- CSS code from Bootply.com editor -->

<style type="text/css">
/* BOOTSTRAP 3.x GLOBAL STYLES
-------------------------------------------------- */
body {
	padding-bottom: 40px;
	color: #5a5a5a;
}

/* CUSTOMIZE THE CAROUSEL
-------------------------------------------------- */

/* Carousel base class */
.carousel {
	margin-bottom: 30px;
}
/* Since positioning the image, we need to help out the caption */
.carousel-caption {
	z-index: 1;
}

/* Declare heights because of positioning of img element */
.carousel .item {
	height: 275px;
}

/* RESPONSIVE CSS
-------------------------------------------------- */
@media ( min-width : 768px) {
	/* Remve the edge padding needed for mobile */
	.marketing {
		padding-left: 0;
		padding-right: 0;
	}

	/* Navbar positioning foo */
	.navbar-wrapper {
		margin-top: 20px;
		margin-bottom: -90px;
		/* Negative margin to pull up carousel. 90px is roughly margins and height of navbar. */
	}
	/* The navbar becomes detached from the top, so we round the corners */
	.navbar-wrapper .navbar {
		border-radius: 4px;
	}

	/* Bump up size of carousel content */
	.carousel-caption p {
		margin-bottom: 20px;
		font-size: 17px;
		line-height: 1;
	}

	/*   .carousel img {
  position: absolute;
  top: 0;
  left: 0;
  min-height: 400px; 
 
  text-align: center;
    img {
        margin: 0 auto;
    } 
} */
}
</style>

</head>

<body>
	<h4 align="center">
		<i>Healthy Options Around Me</i>
	</h4>

	<!-- Carousel
================================================== -->
	<div id="myCarousel" class="carousel slide">

		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>

		<div class="carousel-inner">
			<div class="item active">
				<img
					src="http://cdn.ohsheglows.com/wp-content/uploads/2010/12/IMG_7688.jpg"
					class="img-responsive center-block" alt="First slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Salad</h1>
						<p>Low Fat</p>
						<p></p>
						<p>
							<a class="btn btn-lg btn-primary" href="http://getbootstrap.com">Visit
								Snack Express</a>
						</p>
					</div>
				</div>
			</div>

			<div class="item">
				<img
					src="http://www.shemazing.net/wp-content/uploads/2015/09/download_17.jpg"
					class="img-responsive center-block" alt="First slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Grilled Chicken Wrap</h1>
						<p>High Protein</p>
						<p>
							<a class="btn btn-lg btn-primary" href="http://getbootstrap.com">Visit
								Raw</a>
					</div>
				</div>
			</div>

			<div class="item">
				<img
					src="http://imparteretete.reginele.ro/wp-content/uploads/2013/11/taitei-cu-ton.jpg"
					class="img-responsive center-block" alt="First slide">
				<div class="container">
					<div class="carousel-caption">
						<h1>Noodles</h1>
						<p>High Carbohydrate</p>
						<p>
							<a class="btn btn-lg btn-primary" href="http://getbootstrap.com">Visit
								Little Asia</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="icon-prev"></span>
		</a> <a class="right carousel-control" href="#myCarousel"
			data-slide="next"> <span class="icon-next"></span>
		</a>
	</div>
	<!-- /.carousel -->


	<!-- Page Content -->
	<div class="container">

		<!-- Marketing Icons Section -->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Welcome to Your Dashboard!</h1>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-check"></i> Favourite Food Outlets
						</h4>
					</div>
					<div class="panel-body">
						<p>
						<li>Uni Bros</li>
						<li>Snack Express</li>
						</p>
						<a href="#" class="btn btn-default">View All</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-gift"></i>My Fitness Information
						</h4>
					</div>
					<div class="panel-body">
						<a href="#" class="btn btn-default">Learn More</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>
							<i class="fa fa-fw fa-compass"></i>Recent Searches
						</h4>
					</div>
					<div class="panel-body">
						<p>
						<li>Low Fat</li>
						<li>High Protein, Low Carb</li> 
						</p>
						<a href="#" class="btn btn-default">Search Now</a>
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Friends Row -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header" align="center">My Friends</h3>
            </div>
     <div class="col-lg-4 col-sm-6 text-center">
                <img class="img-circle img-responsive img-center" src="http://placehold.it/200x200" alt="">
                <h5>John Smith
                </h5>
            </div>
            <div class="col-lg-4 col-sm-6 text-center">
                <img class="img-circle img-responsive img-center" src="http://placehold.it/200x200" alt="">
                <h5>John Smith
                </h5>
            </div>
            <div class="col-lg-4 col-sm-6 text-center">
                <img class="img-circle img-responsive img-center" src="http://placehold.it/200x200" alt="">
                <h5>John Smith
                </h5>
            </div>                            
	<!-- /.row -->
</html>