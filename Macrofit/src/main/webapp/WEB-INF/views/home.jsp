<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Custom StyleSheet -->
<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet">

<title>Home</title>
</head>
<body>
	<div class="container">

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">
			<h1>MacroFit</h1>
			<p>MacroFit aims to classify the dishes available at restaurants in a geographical area on the basis of its
			 Macronutrient component i.e. Fat, Carbohydrates, and Protein.
			</p>
			
			<p> It then allows you to search these dishes based on these classifiers. MacroFit uses the United States 
			Department of Agriculture's Food Database which is the most extensive database of its kind 
			for ingredients/components of food.</p>
			
				<a class="btn btn-lg btn-primary" href="/Healthos/Login"
					role="button">Login or Register! &raquo;</a>
			</p>
		</div>

	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>




