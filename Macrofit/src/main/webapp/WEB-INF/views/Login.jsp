<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/css/signin.css"/>" 
	rel="stylesheet" >
</head>
<body>

<div class="container">

      <form class="form-signin" action="/Login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="username" name="Username" required autofocus />
        
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="Password" required />
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
         
        <button class="btn btn-lg btn-primary btn-block" type="submit" value="Login">Sign in</button>
     	 <a class="btn btn-lg btn-primary btn-block" href="/Healthos/register">Register</a>  
      </form>
      
      
      

    </div> <!-- /container -->
    
    
</body>
</html>