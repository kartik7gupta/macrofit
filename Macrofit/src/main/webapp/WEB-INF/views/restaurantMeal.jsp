<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>User Feed</title>
<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-theme.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/feed.css" />"
 	rel="stylesheet">
<!--<script src="<c:url value="/resources/js/feed.js" />"></script>-->

</head>
<body>


	<!-- Main component for a primary marketing message or call to action -->
	<div class="container">
		<h1 class="text-center">Add a new Meal for ${restaurant.getName()}</h1>
		<h4>Existing Meals</h4>
		<ul>
			<c:forEach var="mealItem" items="${restaurantMeals}">
				<li>
					<a href="/Healthos/admin/restaurant/${restaurant.getId()}/meal/${mealItem.getId()}/add/"><c:out value="${mealItem.getName()}"/></a>
				</li>
			</c:forEach>
		</ul>
		
		<form class="form-signin" action="${formActionUrl}" method="POST">
	        <h4 class="form-signin-heading">Add a New Meal</h4>
	        <label for="name" class="sr-only">Meal Name</label>
	        <input type="text" class="form-control" placeholder="Enter a name for the meal" name="name" required autofocus value="${restaurantMeal.name}"/><br/>
	        
	        <label for="about" class="sr-only">About</label>
	        <input type="text" id="inputAbout" class="form-control" placeholder="Describe the meal" name="about" value="${restaurantMeal.about}" required /><br/>
	        
	        <label for="calorieValue" class="sr-only">Calorie Value</label>
	        <input type="text" class="form-control" placeholder="Enter the amount of calories" name="calorieValue" value="${restaurantMeal.calorieValue}" required autofocus /><br/>
	        
	        <label for="carbValue" class="sr-only">Carbs</label>
	        <input type="text" class="form-control" placeholder="Enter carb value in grams" name="carbValue" value="${restaurantMeal.carbValue}" required autofocus /><br/>
	        
	        <label for="proteinValue" class="sr-only">Protein</label>
	        <input type="text" class="form-control" placeholder="Enter protein value in grams" name="proteinValue" value="${restaurantMeal.proteinValue}" required autofocus /><br/>
	        
	        <label for="fatValue" class="sr-only">Fats</label>
	        <input type="text" class="form-control" placeholder="Enter fat value in grams" name="fatValue" value="${restaurantMeal.fatValue}" required autofocus /><br/>
	        
	        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	         
	        <button class="btn btn-lg btn-primary btn-block" type="submit" value="AddMeal">Save Meal</button>
      	</form>
	</div>

	<script>
		function myFunction()
		{
		    var div = document.createElement("DIV");
		    if (document.body.appendChild(div)!=null){
		    	console.log("Created Div");
		    }
		}
	</script>

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body>