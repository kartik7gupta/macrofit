<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>User Feed</title>
<!-- Bootstrap core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap-theme.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/feed.css" />" rel="stylesheet">
<!--<script src="<c:url value="/resources/js/feed.js" />"></script>-->

</head>
<body>


	<!-- Main component for a primary marketing message or call to action -->
	<div class="container">
		<div class="jumbotron">
			<div class="row">
				<div class="col-md-6">
					<h2>Carbohydrate</h2>
					<h2>Fat</h2>
					<h2>Protein</h2>
				</div>

				<div class="col-md-6">

					<h2>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-primary active"> <input
								type="radio" name="options" id="option2" autocomplete="off"
								checked> Low
							</label> <label class="btn btn-primary"> <input type="radio"
								name="options" id="option3" autocomplete="off"> High
							</label>
						</div>
					</h2>

					<h2>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-primary active"> <input
								type="radio" name="options" id="option2" autocomplete="off"
								checked> Low
							</label> <label class="btn btn-primary"> <input type="radio"
								name="options" id="option3" autocomplete="off"> High
							</label>
						</div>
					</h2>
					<h2>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-primary active"> <input
								type="radio" name="options" id="option2" autocomplete="off"
								checked> Low
							</label> <label class="btn btn-primary"> <input type="radio"
								name="options" id="option3" autocomplete="off"> High
							</label>
						</div>
					</h2>
				</div>

			</div>
			<br />
			<p>
				<a class="btn btn-lg btn-primary" onclick="myFunction()"
					align="right" href="#" role="button">Search</a>
			</p>
		</div>
	</div>

	<div id="dishesList">
		<c:forEach items="${meals}" var="meal">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading">${meal.name}</div>
				<div class="panel-body">About: ${meal.about}</div>

				<!-- List group -->
				<ul class="list-group">

					<li class="list-group-item">${meal.restaurant.name}</li>

					<li class="list-group-item"><span class="badge">${meal.calorieValue} calories</span>
						Calories</li>

					<li class="list-group-item"><span class="badge">${meal.fatValue} g</span>
						Fat</li>

					<li class="list-group-item"><span class="badge">${meal.carbValue} g</span>
						Carbohydrate</li>

					<li class="list-group-item"><span class="badge">${meal.proteinValue} g</span>
						Protein</li>
				</ul>
			</div>
		</c:forEach>
	</div>


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

</body>