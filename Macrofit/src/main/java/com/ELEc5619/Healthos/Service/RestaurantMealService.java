package com.ELEc5619.Healthos.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.ELEc5619.Healthos.Business.RestaurantMealDAO;
import com.ELEc5619.Healthos.Model.Restaurant;
import com.ELEc5619.Healthos.Model.RestaurantMeal;

@Service
public class RestaurantMealService {

	@Autowired
	RestaurantMealDAO restaurantPersistence;

	public void addRestaurant(Restaurant restaurant) {
		restaurantPersistence.addRestaurant(restaurant);
	}
	
	public void addRestaurantMeal(RestaurantMeal restaurantMeal) {
		restaurantPersistence.addRestaurantMeal(restaurantMeal);
	}
	
	public void updateRestaurantMeal(RestaurantMeal restaurantMeal) {
		restaurantPersistence.updateRestaurantMeal(restaurantMeal);
	}
	
	public Restaurant getRestaurant(int id) {
		return this.restaurantPersistence.getRestaurant(id);
	}
	
	public RestaurantMeal getRestaurantMeal(Integer id) {
		return this.restaurantPersistence.getRestaurantMeal(id);
	}
	
	public ArrayList<RestaurantMeal> getRestaurantMeals(Integer restaurant_id) {
		return this.restaurantPersistence.getRestaurantMeals(restaurant_id);
	}
	
	public ArrayList<RestaurantMeal> getAllRestaurantMeals() {
		return this.restaurantPersistence.getAllRestaurantMeals();
	}
}
