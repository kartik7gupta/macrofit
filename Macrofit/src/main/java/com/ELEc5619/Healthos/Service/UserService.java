package com.ELEc5619.Healthos.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ELEc5619.Healthos.Business.UserDAO;
import com.ELEc5619.Healthos.Model.User;

@Service
public class UserService {
	
	@Autowired
	UserDAO userPersistence;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	public void addUser(User user)
	{
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userPersistence.add(user);
	}
	

}
