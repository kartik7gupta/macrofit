package com.ELEc5619.Healthos.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Restaurant")
public class Restaurant {

	/** Column Declarations **/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "Name")
	private String name;

	@Column(name = "About")
	private String about;

	@Column(name = "Address")
	private String address;

	@Column(name = "OpeningHours")
	private String openingHours;

	@Column(name = "Category")
	private int category;

	/** Constructors **/
	public Restaurant() {
	}

	public Restaurant(String name, String about, String address, String openingHours, int category) {
		this.name = name;
		this.about = about;
		this.address = address;
		this.openingHours = openingHours;
		this.category = category;
	}

	/** Setters and getters **/
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return this.about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String setOpeningHours() {
		return this.openingHours;
	}

	public void getOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}

	public int getCategory() {
		return this.category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
}
