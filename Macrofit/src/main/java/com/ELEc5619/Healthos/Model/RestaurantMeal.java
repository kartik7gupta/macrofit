package com.ELEc5619.Healthos.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RestaurantMeal")
public class RestaurantMeal {
	
	/** Column Declarations **/
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne
	@JoinColumn(name = "RestaurantId")
	private Restaurant restaurant;
	
	@Column(name = "Name")
	private String name;

	@Column(name = "About")
	private String about;

	@Column(name = "Category")
	private int category;

	@Column(name = "CalorieValue")
	private int calorieValue;
	
	@Column(name = "CarbValue")
	private int carbValue;
	
	@Column(name = "ProteinValue")
	private int proteinValue;
	
	@Column(name = "FatValue")
	private int fatValue;
	
	/** Constructors **/
	public RestaurantMeal() {
	}
	
	public RestaurantMeal(String name, String about, int category) {
		this.name = name;
		this.about = about;
		this.category = category;
	}
	
	/** Setters and getters **/
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbout() {
		return this.about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public int getCategory() {
		return this.category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
	
	public void setCalorieValue(int calorieValue) {
		this.calorieValue = calorieValue;
	}
	
	public int getCalorieValue() {
		return this.calorieValue;
	}
	
	public void setCarbValue(int carbValue) {
		this.carbValue = carbValue;
	}
	
	public int getCarbValue() {
		return this.carbValue;
	}
	
	public void setProteinValue(int proteinValue) {
		this.proteinValue = proteinValue;
	}
	
	public int getProteinValue() {
		return this.proteinValue;
	}
	
	public void setFatValue(int fatValue) {
		this.fatValue = fatValue;
	}
	
	public int getFatValue() {
		return this.fatValue;
	}
}
