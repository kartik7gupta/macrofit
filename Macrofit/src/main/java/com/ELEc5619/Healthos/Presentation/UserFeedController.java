package com.ELEc5619.Healthos.Presentation;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ELEc5619.Healthos.Model.Restaurant;
import com.ELEc5619.Healthos.Model.RestaurantMeal;
import com.ELEc5619.Healthos.Service.RestaurantMealService;

@Controller

public class UserFeedController {

	private static final Logger logger = LoggerFactory.getLogger(UserFeedController.class);
	
	@Autowired
	RestaurantMealService mealService;

	
	@RequestMapping(value="/feed",method=RequestMethod.GET)
	public String meals(Locale locale, Model model) {
		
		
		
		ArrayList<RestaurantMeal> meals = mealService.getAllRestaurantMeals();
			model.addAttribute("meals",meals);
		

		return "feed";
	}

}
