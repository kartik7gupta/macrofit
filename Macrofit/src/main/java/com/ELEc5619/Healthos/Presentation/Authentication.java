package com.ELEc5619.Healthos.Presentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ELEc5619.Healthos.Model.User;
import com.ELEc5619.Healthos.Service.UserService;

@Controller
public class Authentication {
	
	@Autowired
	UserService service;
	
	@RequestMapping(value = "/register",method = RequestMethod.GET)
	public String Register()
	{
		return "Register";
	}
	
	@RequestMapping(value= "/register", method = RequestMethod.POST)
	public String validateForm(@ModelAttribute("user") User user)
	{
		service.addUser(user);
		return "feed";
	}
	
	
	@RequestMapping(value="/Login",method = RequestMethod.GET)
	public String login()
	{
		return "Login";
	}

}
