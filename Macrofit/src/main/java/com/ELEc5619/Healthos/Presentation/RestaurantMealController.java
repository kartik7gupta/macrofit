package com.ELEc5619.Healthos.Presentation;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ELEc5619.Healthos.Model.Restaurant;
import com.ELEc5619.Healthos.Model.RestaurantMeal;
import com.ELEc5619.Healthos.Service.RestaurantMealService;

@Controller
public class RestaurantMealController {

	@Autowired
	RestaurantMealService service;
	
	@RequestMapping(value = "/admin/restaurant/add", method = RequestMethod.GET)
	public String addRestaurant() {
//		 Temporarily add a new restaurant 
		Restaurant r = new Restaurant("Subway", "Sandwich", "Sydney University, 2007", "9am - 6pm, Mon-Saturday", 1);
		service.addRestaurant(r);
		System.out.println("About to display the add restaurant view");
		return "restaurant/addRestaurant";
	}

//	@RequestMapping(value = "/admin/restaurant/add", method = RequestMethod.POST)
//	public String addRestaurantValidateForm(@ModelAttribute("Restaurant") Restaurant restaurant) {
//		System.out.println("Form submission received for new restaurant!");
//		// service
//		return "restaurant/addRestaurant";
//	}

	@RequestMapping(value = "/admin/restaurant/{restaurant_id}/meal/add", method = RequestMethod.GET)
	public String addMeal(@PathVariable String restaurant_id, Map<String, Object> map) {

		// Fetch the restaurant
		Restaurant restaurant = service.getRestaurant(Integer.parseInt(restaurant_id));
		ArrayList<RestaurantMeal> restaurantMeals = service.getRestaurantMeals(restaurant.getId());
		
		map.put("restaurant", restaurant);
		System.out.println("========");
		System.out.println(restaurantMeals);
		map.put("restaurantMeals", restaurantMeals);
		String formActionUrl = "/Healthos/admin/restaurant/" + restaurant.getId().toString() + "/meal/add/";
		map.put("formActionUrl", formActionUrl);
		// System.out.println(restaurant.getName());
		return "restaurantMeal";
	}

	@RequestMapping(value = "/admin/restaurant/{restaurant_id}/meal/add", method = RequestMethod.POST)
	public String addMealValidateForm(@ModelAttribute("Restaurant") Restaurant restaurant,
			@PathVariable String restaurant_id,
			@RequestParam(value="name", required=true) String name,
			@RequestParam(value="about", required=true) String about,
			@RequestParam(value="calorieValue", required=true) String calorieValue,
			@RequestParam(value="fatValue", required=true) String fatValue,
			@RequestParam(value="proteinValue", required=true) String proteinValue,
			@RequestParam(value="carbValue", required=true) String carbValue
			) {
		System.out.println("Form submission received for new meal!");
		RestaurantMeal meal = new RestaurantMeal();
		meal.setRestaurant(service.getRestaurant(Integer.parseInt(restaurant_id)));
		meal.setName(name);
		meal.setAbout(about);
		meal.setCalorieValue(Integer.parseInt(calorieValue));
		meal.setCarbValue(Integer.parseInt(carbValue));
		meal.setProteinValue(Integer.parseInt(proteinValue));
		meal.setFatValue(Integer.parseInt(fatValue));
		service.addRestaurantMeal(meal);
		return "restaurantMeal";
	}
	
	@RequestMapping(value = "/admin/restaurant/{restaurant_id}/meal/{meal_id}/add", method = RequestMethod.GET)
	public String updateMeal(
			@PathVariable String restaurant_id,
			@PathVariable String meal_id,
			Map<String, Object> map) {

		// Fetch the restaurant
		Restaurant restaurant = service.getRestaurant(Integer.parseInt(restaurant_id));
		RestaurantMeal restaurantMeal = service.getRestaurantMeal(Integer.parseInt(meal_id));
		map.put("restaurant", restaurant);
		map.put("restaurantMeal", restaurantMeal);
		ArrayList<RestaurantMeal> restaurantMeals = service.getRestaurantMeals(restaurant.getId());		
		map.put("restaurantMeals", restaurantMeals);
		String formActionUrl = "/Healthos/admin/restaurant/" + restaurant.getId().toString() + "/meal/" + Integer.toString(restaurantMeal.getId()) + "/add/";
		map.put("formActionUrl", formActionUrl);
		// System.out.println(restaurant.getName());
		return "restaurantMeal";
	}

	@RequestMapping(value = "/admin/restaurant/{restaurant_id}/meal/{meal_id}/add", method = RequestMethod.POST)
	public String updateMealForm(@ModelAttribute("Restaurant") Restaurant restaurant,
			@PathVariable String restaurant_id,
			@PathVariable String meal_id,
			Map<String, Object> map,
			@RequestParam(value="name", required=true) String name,
			@RequestParam(value="about", required=true) String about,
			@RequestParam(value="calorieValue", required=true) String calorieValue,
			@RequestParam(value="fatValue", required=true) String fatValue,
			@RequestParam(value="proteinValue", required=true) String proteinValue,
			@RequestParam(value="carbValue", required=true) String carbValue
			) {
		System.out.println("Form submission received for updating meal!");
		RestaurantMeal meal = service.getRestaurantMeal(Integer.parseInt(meal_id));
		meal.setRestaurant(service.getRestaurant(Integer.parseInt(restaurant_id)));
		meal.setName(name);
		meal.setAbout(about);
		meal.setCalorieValue(Integer.parseInt(calorieValue));
		meal.setCarbValue(Integer.parseInt(carbValue));
		meal.setProteinValue(Integer.parseInt(proteinValue));
		meal.setFatValue(Integer.parseInt(fatValue));
		service.updateRestaurantMeal(meal);
		
		map.put("restaurant", meal.getRestaurant());
		map.put("restaurantMeal", meal);
		ArrayList<RestaurantMeal> restaurantMeals = service.getRestaurantMeals(meal.getRestaurant().getId());		
		map.put("restaurantMeals", restaurantMeals);
		String formActionUrl = "/Healthos/admin/restaurant/" + meal.getRestaurant().getId().toString() + "/meal/" + Integer.toString(meal.getId()) + "/add/";
		map.put("formActionUrl", formActionUrl);

		return "restaurantMeal";
	}
}
