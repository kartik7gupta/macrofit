package com.ELEc5619.Healthos.Business;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ELEc5619.Healthos.Model.Restaurant;
import com.ELEc5619.Healthos.Model.RestaurantMeal;


@Repository
public class RestaurantMealDAO {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public void addRestaurant(Restaurant restaurant) {
		sessionFactory.getCurrentSession().save(restaurant);
		sessionFactory.getCurrentSession().flush();
	}
	
	@Transactional
	public void addRestaurantMeal(RestaurantMeal restaurantMeal) {
		sessionFactory.getCurrentSession().save(restaurantMeal);
	}
	
	@Transactional
	public void updateRestaurantMeal(RestaurantMeal restaurantMeal) {
		sessionFactory.getCurrentSession().update(restaurantMeal);
	}

	@Transactional
	public Restaurant getRestaurant(Integer id) {
		Restaurant restaurant = (Restaurant) sessionFactory.getCurrentSession().get(Restaurant.class, new Integer(id));
		return restaurant;
	}
	
	@Transactional
	public RestaurantMeal getRestaurantMeal(Integer id) {
		RestaurantMeal restaurantMeal = (RestaurantMeal) sessionFactory.getCurrentSession().get(RestaurantMeal.class, id);
		return restaurantMeal;
	}
	
	@Transactional
	public ArrayList<RestaurantMeal> getRestaurantMeals(Integer restaurant_id) {
		String SQL_QUERY = "FROM RestaurantMeal meal WHERE meal.restaurant = " + Integer.toString(restaurant_id);
		Query query = sessionFactory.getCurrentSession().createQuery(SQL_QUERY);
		ArrayList<RestaurantMeal> restaurantMeals = new ArrayList<RestaurantMeal>();
		for (Iterator it=query.iterate();it.hasNext();) {
			RestaurantMeal meal = (RestaurantMeal) it.next();
			System.out.println(meal.getName());
			restaurantMeals.add(meal);
		}
		return restaurantMeals;
	}
	
	@Transactional
	public ArrayList<RestaurantMeal> getAllRestaurantMeals() {
		String SQL_QUERY = "FROM RestaurantMeal meal";
		Query query = sessionFactory.getCurrentSession().createQuery(SQL_QUERY);
		ArrayList<RestaurantMeal> restaurantMeals = new ArrayList<RestaurantMeal>();
		for (Iterator it=query.iterate();it.hasNext();) {
			RestaurantMeal meal = (RestaurantMeal) it.next();
			System.out.println(meal.getName());
			restaurantMeals.add(meal);
		}
		return restaurantMeals;
	}
}
