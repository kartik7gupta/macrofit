package com.ELEc5619.Healthos.Business;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ELEc5619.Healthos.Model.User;

@Repository
public class UserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Transactional
	public void add(User user) {
		
		sessionFactory.getCurrentSession().save(user);
		
	}
}
