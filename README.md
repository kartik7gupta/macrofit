# MacroFit

MacroFit aims to classify the dishes available at restaurants in a geographical area on the basis of its Macronutrient component i.e. Fat, Carbohydrates, and Protein. It then allows you to search these dishes based on these classifiers. MacroFit uses the United States Department of Agriculture's Food Database which is the most extensive database of its kind for ingredients/components of food. 

##Team Members
| Name             |Email                       | Documentation and Component               |
| -------------    | -------------------------- | ----------------------------------------------------------------------------------------|
| Kartik Gupta    | kgup7941@uni.sydney.edu.au | [User Feed](https://bitbucket.org/kartik7gupta/macrofit/wiki/User%20Feed), [Wiki](/macrofit/wiki)                                    |
| Kiran Mani Mohan  | kmoh3793@uni.sydney.edu.au | [User Dashboard](https://bitbucket.org/kartik7gupta/macrofit/wiki/User%20Dashboard)                                                  |
| Anam Ahmed     | aahm7054@uni.sydney.edu.au | [Restaurants](https://bitbucket.org/kartik7gupta/macrofit/wiki/Restaurant%20Profiles)                                                 |
| Tushar Bhatia    | tushar@bhatia.io | [Restaurant Meal Management](https://bitbucket.org/kartik7gupta/macrofit/wiki/Restaurant%20Meal%20Management)
| Patrice Jonathan k/nell    | pkne1753@uni.sydney.edu.au | [Integrating Foreign API's](https://bitbucket.org/kartik7gupta/macrofit/wiki/Integrating%20Foreign%20API's)                                       |

## Development Schedule

The Agile approach in particular SCRUM was followed. This was enforced by the [Scrum Master](mailto:kartik7gupta@gmail.com) 

* Sprints - [bitbucket.org/kartik7gupta/macrofit/wiki/Sprints](https://bitbucket.org/kartik7gupta/macrofit/wiki/Sprints)

##Quality Practices

While developing MacroFit it was ensured that quality practices were followed. Code reviews, pull requests, pair programming, unit tests are some of the procedures that were taken as standard while developing MacroFit. More information can be found on our Quality Page:

* [Quality Practices](https://bitbucket.org/kartik7gupta/macrofit/wiki/Team%20Quality%20Practices)
* [Test Specifications](https://bitbucket.org/kartik7gupta/macrofit/wiki/Test%20Specifications%20for%20MacroFit%20)
* [Commits](https://bitbucket.org/kartik7gupta/macrofit/commits)

## System Requirements

1. Spring Tool Suite 3.7 or greater
1. Java 1.7 or greater
1. Git 1.5 or greater
1. The latest version of MySQL Database
1. Apache Tomcat V8.0

## Installation

1. Download or clone [this](/macrofit) repository
> To clone use this command:
> ``
> git clone git@bitbucket.org:kartik7gupta/macrofit.git
> ``
1. Open Spring Tool Suite
1. Import Cloned repository into workspace
1. Set Apache Tomcat as default server for MacroFit
1. Open MySQL
1. Run the seed file to create the schema and populate it with sample data
1. Click Run
1. Open Browser and navigate to localhost:8080

##Support

If you are having trouble setting MacroFit up or using it, please email us at [kartik7gupta@gmail.com](mailto:kartik7gupta@gmail.com)

## Contribute

* Issue Tracker - [bitbucket.org/kartik7gupta/macrofit/issues](https://bitbucket.org/kartik7gupta/macrofit/issues) 
* Source Code - [bitbucket.org/kartik7gupta/macrofit/](https://bitbucket.org/kartik7gupta/macrofit/)
* [Individual Component Template](https://bitbucket.org/kartik7gupta/macrofit/wiki/Individual%20Component%20Template)

## Libraries Used

We like to acknowledge our use of the following libraries. The libraries used are open to public use and no licenses have been violated while developing this project

* Oracle Java 1.7
* Twitter Bootstrap
* Sitemesh

## License

MacroFit is licensed under the BSD License. This is a positive computing project developed for ELEC5619 at the University of Sydney.